from alpine

MAINTAINER Tomas Sapak <sapakt@ics.muni.cz>

LABEL Description="ansible-lint"

RUN apk add --update py-pip gcc python2-dev musl-dev libffi-dev openssl-dev make \
    && pip install ansible-lint
